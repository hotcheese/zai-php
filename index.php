<?php require('baseLayoutTop.php');
session_start(); ?>
<h1>Index page</h1>
<br>
<a class="menu-item" href="private.php">Webpage for users</a>
<br>
<a class="menu-item" href="public.php">Webpage for everybody</a>
<br>
<?php if (!isset($_SESSION['username'])) { ?>
    <a class="menu-item" href="loginform.php">Log in</a>
    <br>
    <a class="menu-item" href="signupform.php">Sign up</a>
<?php } ?>
<?php if (isset($_SESSION['username'])) { ?>
    <br><br><br>
    <a class="menu-item" href="delete_account.php">Delete account</a>
<?php } ?>
<?php require('baseLayoutBottom.php'); ?>
