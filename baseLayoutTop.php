<!DOCTYPE html>
<html>
<head>
    <title>Artur Sulej - Projekt ZAI</title>
    <link href='https://fonts.googleapis.com/css?family=Fredoka+One' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="favicon.png">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script type="text/javascript" src="script.js"></script>
</head>
<body>
<header class="top-bar">
    <?php
    if (basename($_SERVER['PHP_SELF']) != 'index.php') {
        ?>
        <a id="index" href="index.php"><< Index</a>
        <?php
    }
    ?>
    <h1>ZAI</h1>
</header>
<br>
<div id="main">
    <?php
    session_start();
    if (isset($_SESSION['username'])) {
        require('hello.inc');
    } ?>
<!--Content here.-->
<!--baseLayoutBottom.php-->