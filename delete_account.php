<?php
require('db_connector.inc');
require('auth.inc');

if(!auth()) {
    echo "You must be logged in to delte your account.";
    header("refresh:2;url=loginform.php");
    exit;
}
$db_link = get_mysql_connection();

session_start();
$username = $_SESSION['username'];
$delete_user = "DELETE FROM users WHERE username='$username' LIMIT 1;";

$result = $db_link->query($delete_user);

$_SESSION['username'] = NULL;
$_SESSION['access_token'] = NULL;

echo "Your account has been deleted.";

header("refresh:2;url=index.php");
?>



