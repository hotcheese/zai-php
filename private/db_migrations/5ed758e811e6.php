<?php
require 'db_connector.inc';

$sql_drop_users = "DROP TABLE IF EXISTS users;";
$sql_drop_loggedusers = "DROP TABLE IF EXISTS loggedusers;";

$sql_create_users = "CREATE TABLE users( " .
    "id INT NOT NULL AUTO_INCREMENT, " .
    "email VARCHAR(100) NOT NULL, " .
    "username VARCHAR(100) NOT NULL, " .
    "password VARCHAR(100) NOT NULL, " .
    "PRIMARY KEY ( id )); ";

$sql_create_loggedusers = "CREATE TABLE loggedusers( " .
    "id INT NOT NULL AUTO_INCREMENT, " .
    "access_token VARCHAR(100), " .
    "user_id INT NOT NULL, " .
    "INDEX user_ind (user_id), " .
    "PRIMARY KEY ( id )," .
    "FOREIGN KEY (user_id)" .
    "    REFERENCES users (id)" .
    "    ON DELETE CASCADE);";

$link = get_mysql_connection();
echo $link->query($sql_drop_users);
echo $link->query($sql_drop_loggedusers);
echo $link->query($sql_create_users);
echo $link->query($sql_create_loggedusers);
?>