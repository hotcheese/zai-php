<?php require('baseLayoutTop.php');
require('auth.inc'); ?>
<h1>Page for registered users</h1>
<?php
session_start();
if (!auth()) { ?>
    <h3>Please log in to view the content.</h3>
    <?php require 'loginform.inc'; ?>
<?php } else { ?>
    <h3>Premium stuff only for registered users!</h3>
    <img src="public/tiger.jpg"/>
<?php } ?>
<?php require('baseLayoutBottom.php'); ?>
