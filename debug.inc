<?php
if (!function_exists('debug_to_console')) {
    function debug_to_console($data)
    {
        if (is_array($data))
            $output = "<script>console.log( 'Debug Array(PHP): " . implode(',', $data) . "' );</script>";
        else
            $output = "<script>console.log( 'Debug Object(PHP): " . $data . "' );</script>";

        echo $output;
    }
}

?>