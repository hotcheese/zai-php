<?php
if (!function_exists('get_mysql_connection')) {
    function get_mysql_connection()
    {
        $isProduction = getenv("PRODUCTION");
        if ($isProduction) {
            $url = parse_url(getenv("CLEARDB_DATABASE_URL"));

            $username = $url["user"];
            $password = $url["pass"];
            $server = $url["host"];
            $db = substr($url["path"], 1);

            $link = new mysqli($server, $username, $password, $db);
            if (!$link || !$server || !$db) {
                echo "MySQL connection failed. " . mysqli_connect_error() . PHP_EOL;
                return NULL;
            }
            return $link;
        } else {
            return new mysqli("localhost", "root", "ultra", "zai");
        }
    }
}
?>