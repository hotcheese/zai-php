function validateForm() {
    var username = document.getElementById('username');
    var password = document.getElementById('password');
    var email = document.getElementById('email');
    var confirmation_pass = document.getElementById('passwordrepeat');

    if (password.value !== confirmation_pass.value) {
        alert("Passwords don't match");
        password.value = '';
        confirmation_pass.value = '';
        return false;
    } else if (username.value.length == 0) {
        alert("Username can't be empty");
        return false;
    } else if (email.value.length == 0) {
        alert("Email can't be empty");
        return false;
    } else if (!validateEmail(email.value)) {
        alert("Email is incorrect");
        return false;
    } else if (password.value.length == 0) {
        alert("Password can't be empty");
        return false;
    } else {
        return true;
    }
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}