<?php require('baseLayoutTop.php'); ?>
<br>
<form id='register' action='register.php' onsubmit='return validateForm();' method='post' style="width: 440px;">
    <fieldset>
        <legend>Create new account:</legend>
        <table>
            <tr>
                <td>
                    Username (login):
                </td>
                <td>
                    <input type="text" name="username" id="username" required/>
                </td>
            </tr>
            <tr>
                <td>
                    Email:
                </td>
                <td>
                    <input type="email" name="email" id="email" required/>
                </td>
            </tr>
            <tr>
                <td>
                    Password:
                </td>
                <td>
                    <input type="password" name="password" id="password" required/>
                </td>
            </tr>
            <tr>
                <td>
                    Repeat password:
                </td>
                <td>
                    <input type="password" name="passwordrepeat " id="passwordrepeat" required/><br>
                </td>
            </tr>
        </table>
        <p><input type="submit" value="Submit"/></p>
    </fieldset>
</form>
<?php require('baseLayoutBottom.php'); ?>
